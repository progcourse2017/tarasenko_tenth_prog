#include "stdafx.h"
#include "zoo.h"

zooShop::zooShop() {};
zooShop::~zooShop() {};

zooShop::zooShop(string newAnimal, string newGender, string newName, unsigned newCost, unsigned newCount)
{
	animal = newAnimal;
	gender = newGender;
	name = newName;
	cost = newCost;
	count = newCount;
};
zooShop::zooShop(const zooShop &obj)
{
	animal = obj.animal;
	gender = obj.gender;
	name = obj.name;
	cost = obj.cost;
	count = obj.count;
};

void zooShop::setData(string newAnimal, string newGender, string newName, unsigned newCost, unsigned newCount)
{
	animal = newAnimal;
	gender = newGender;
	name = newName;
	cost = newCost;
	count = newCount;
};

void zooShop::getData()
{

	cout << "Animal: " << animal
		<< "\nGender: " << gender
		<< "\nName: " << name
		<< "\nCost: " << cost
		<< "\nCount: " << count << endl;

};

