#include "stdafx.h"
#include "WildAnimal.h"
void WildAnimal::setData(string newAnimal, string newGender, string newName, unsigned newCost, unsigned newCount, string newCharacter)
{
	animal = newAnimal;
	gender = newGender;
	name = newName;
	cost = newCost;
	count = newCount;
	character = newCharacter;
}
void WildAnimal::getData()
{
	cout << "Animal: " << animal
		<< "\nGender: " << gender
		<< "\nName: " << name
		<< "\nCost: " << cost
		<< "\nCount: " << count
		<< "\nCharacter: " << character << endl;
};
void WildAnimal::getCharacter()
{
	cout << "Character: " << character << endl;
}