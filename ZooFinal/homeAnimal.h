#include "stdafx.h"
#include <string>
#include <iostream>
#include "zoo.h"
using namespace std;


class homeAnimal : protected zooShop
{
private:
	unsigned int age;
public:
	homeAnimal() {};
	homeAnimal(string newAnimal, string newGender, string newName, unsigned newCost, unsigned newCount, unsigned int newAge) :zooShop() { age = newAge; };
	homeAnimal(const zooShop &obj, const homeAnimal &obj1) :zooShop() { age = obj1.age; };
	~homeAnimal() {};

	void setData(string newAnimal, string newGender, string newName, unsigned newCost, unsigned newCount, unsigned age);
	void getData();
	void changeAge(unsigned newAge);
};