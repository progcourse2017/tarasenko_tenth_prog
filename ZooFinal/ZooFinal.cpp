#include "stdafx.h"
#include <iostream>
#include "WildAnimal.h"
#include "homeAnimal.h"
#include "zoo.h"


using namespace std;

int main()
{
	setlocale(LC_ALL, "russian");
	homeAnimal pet1;
	pet1.setData("кошка", "m", "Vasya", 100, 1000, 2);
	WildAnimal pet2;
	pet2.setData("tiger", "g", "Mark", 1, 10000, "bad");
	pet1.getData();
	pet2.getData();
	pet1.changeAge(10);
	pet1.getData();
	pet2.getCharacter();

	system("pause");

	return 0;
}

