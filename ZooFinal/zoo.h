#pragma once
#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;


class zooShop
{
protected:
	std::string animal;
	std::string gender;
	std::string name;
	unsigned cost;
	unsigned count;
	
public:
	zooShop();
	zooShop(string newAnimal, string newGender, string newName, unsigned newCost, unsigned newCount);
	zooShop(const zooShop &obj);
	~zooShop();

	virtual void setData(string newAnimal, string newGender, string newName, unsigned newCost, unsigned newCount);
	virtual void getData();


	
};
