#pragma once
#include "stdafx.h"
#include <string>
#include "zoo.h"
#include <iostream>
using namespace std;


class WildAnimal :protected zooShop
{
private:
	string character;
public:
	WildAnimal() :zooShop() {};
	WildAnimal(string newAnimal, string newGender, string newName, unsigned newCost, unsigned newCount, string newCharacter) :zooShop() { character = newCharacter; };
	WildAnimal(const zooShop &obj, const WildAnimal &obj1) :zooShop() { character = obj1.character; };
	~WildAnimal() {};

	void setData(string newAnimal, string newGender, string newName, unsigned newCost, unsigned newCount, string character);
	void getData();
	void getCharacter();


};