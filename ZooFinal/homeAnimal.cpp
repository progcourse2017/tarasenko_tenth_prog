#include "stdafx.h"
#include "homeAnimal.h"

void homeAnimal::setData(string newAnimal, string newGender, string newName, unsigned newCost, unsigned newCount, unsigned newage)
{
	animal = newAnimal;
	gender = newGender;
	name = newName;
	cost = newCost;
	count = newCount;
	age = newage;
}
void homeAnimal::getData()
{
	cout << "Animal: " << animal
		<< "\nGender: " << gender
		<< "\nName: " << name
		<< "\nCost: " << cost
		<< "\nCount: " << count
		<< "\nAge: " << age << endl;
};
void homeAnimal::changeAge(unsigned newAge)
{
	age = newAge;
}